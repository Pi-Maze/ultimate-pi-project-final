from flask import Flask, render_template


""" The Ultimate Py Game Uses The Python Flask Library providing users to enjoy at least 4 games .
    The four games ultimately are not played using the keyboard controls. Instead, an ADXL345 accelerometer is connected
    to a Raspberry Pi via a breadboard. The GPIO pins are used to establish connection. The accelerometer generates readings based on motion and gravity. The ADXL345 Accelerometer library
    is used to interpret the readings into digital format and display the x,y,z axis acceleration values.
    The ADXL345 library depends on the following Libraries which are part of this Flask project: Adafruit_ADXL345 folder,
    Adafruit_GPIO, Adafruit_PureIO.
    
    The Ultimate Pi Game is a web based app with carefully selected games which are solely controlled based on movements.
    The Collection consists on two simple maze games, a racing game and a ball maze game(which depends on Webgl).
    
    The web depends on some components of Bootstrap v4 Animate CSS for animations and the Jquery javascript library v3.
    
    Every game instance requires movements in the LEFT, FORWARD, BACK AND RIGHT direction to play the game. Using AJAX, 
    the browser fetches the accelerometer readings every number of milliseconds. This readings are the ones generated by
    the user whiles the accelerometer is tilted to play the game.
    The readings in the x,y,z format are re-evaluated in the Flask application to generate a concrete position in the form,
    LEFT, FORWARD, BACK OR RIGHT. The position is sent back to the browser and is processed to position the desired position
    of the game been played.
    
    This project was done during the Global Code Summer Camp,  2018, held at the University of Cape Coast.
    The Team members include:
    
    Authors: Innocent Ababio, Amoo Edward and Francis Deh.
    Date: 16th July - 20th July, Monday - Friday, 2018. 9am - 4pm each day.
"""



# Import the ADXL345 module.
import Adafruit_ADXL345

app = Flask(__name__)


# Create an ADXL345 instance.
accel = Adafruit_ADXL345.ADXL345()

# Alternatively you can specify the device address and I2C bus with parameters:
#accel = Adafruit_ADXL345.ADXL345(address=0x54, busnum=2)

# You can optionally change the range to one of:
#  - ADXL345_RANGE_2_G   = +/-2G (default)
#  - ADXL345_RANGE_4_G   = +/-4G
#  - ADXL345_RANGE_8_G   = +/-8G
#  - ADXL345_RANGE_16_G  = +/-16G
# For example to set to +/- 16G:
#accel.set_range(Adafruit_ADXL345.ADXL345_RANGE_16_G)

# Or change the data rate to one of:
#  - ADXL345_DATARATE_0_10_HZ = 0.1 hz
#  - ADXL345_DATARATE_0_20_HZ = 0.2 hz
#  - ADXL345_DATARATE_0_39_HZ = 0.39 hz
#  - ADXL345_DATARATE_0_78_HZ = 0.78 hz
#  - ADXL345_DATARATE_1_56_HZ = 1.56 hz
#  - ADXL345_DATARATE_3_13_HZ = 3.13 hz
#  - ADXL345_DATARATE_6_25HZ  = 6.25 hz
#  - ADXL345_DATARATE_12_5_HZ = 12.5 hz
#  - ADXL345_DATARATE_25_HZ   = 25 hz
#  - ADXL345_DATARATE_50_HZ   = 50 hz
#  - ADXL345_DATARATE_100_HZ  = 100 hz (default)
#  - ADXL345_DATARATE_200_HZ  = 200 hz
#  - ADXL345_DATARATE_400_HZ  = 400 hz
#  - ADXL345_DATARATE_800_HZ  = 800 hz
#  - ADXL345_DATARATE_1600_HZ = 1600 hz
#  - ADXL345_DATARATE_3200_HZ = 3200 hz
# For example to set to 6.25 hz:

#accel.set_data_rate(Adafruit_ADXL345.ADXL345_DATARATE_6_25HZ)


#This is a default position which is not neccessary
position = ''

#the root of the application displays the introductory page which provides links to the various games
@app.route('/')
def root():
    return render_template('intro.html')

#this route leads to the first maze game, a simple maze game
@app.route('/maze1')
def maze1():
    return render_template('maze1.html')

#this route leads to the second maze game intended for intermediate game play
@app.route('/maze2')
def maze2():
    return render_template('maze2.html')

#this route leads to a ball maze game
@app.route('/ball')
def ball():
    return render_template('ball.html')


""" 
    The Car racing game has 4 levels. The game could be played with only a straight road, curved roads, or hills.
    There is a final level which could be played in a city.
    The routes to each of the games is provided below:
"""
@app.route('/straight')
def racer_straight():
    return render_template('v1.straight.html')

@app.route('/curves')
def racer_curves():
    return render_template('v2.curves.html')

@app.route('/hills')
def racer_hills():
    return render_template('v3.hills.html')

@app.route('/final')
def racer_final():
    return render_template('v4.final.html')

@app.route('/tetris')
def tetris():
    return render_template('tetris.html')

"""
    This route is an endpoint for AJAX requests. When the browser sends an AJAX call, this endpoint
    retrieves the accelerometer readings as tilted by the user/player of the game. The readings are
    re-evaluated to mean 'LEFT, RIGHT, FORWARD, BACK'. This re-evaluated position is sent back to the 
    function that executed the AJAX call. The position is used to determine the position of the object in
    any of the above mentioned game.
    
    In each html file that serve the games, there is a javascript function to send the AJAX request.
    The function name is 'getDataFromServer'. It uses JQUERY javascript library version 3  to execute AJAX
    request. 
"""
@app.route('/json')
def index():
    x, y, z = accel.read()
    print('X={0}, Y={1}, Z={2}'.format(x, y, z))

    if x > 0 and y > 0 and z > 0:
        position = "Forward"
    elif x > 0 and y < 0 and z < 0:
        position = "Right"
    elif x < 0 and y > 0 and z > 0:
        position = "Left"
    elif x < 0 and y < 0 and z < 0:
        position = "Back"
    elif x > 0 and y > 0 and z < 0:
        position = "Forward"
    elif x > 0 and y < 0 and z > 0:
        position = "Right"
    elif x < 0 and y < 0 and z > 0:
        position = "Back"
    elif x < 0 and y > 0 and z < 0:
        position = "Left"
    else:
        position = "No direction"

    return position   


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8000)


